﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSBlogModel
{
    public class ArticleRes
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public List<Tag> Tages { get; set; }
        public Delta Paragraphs { get; private set; }
    }

    public class AddArticleRes
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public List<Tag> Tages { get; set; }
        public Guid BlogerID { get; set; }
    }

    public class Article
    {
        public Article() { }
       
        public Article(string title, List<Tag> tags, string subtitle)
        {
            Title = title;
            Subtitle = subtitle;

            CreateAt = DateTime.Now;
            UpdateAt = DateTime.Now;

            Tags = tags;

            PreviewContent = new PreviewContent(title, tags,  "defualt image");
        }

        [Key]
        public Guid ArticleID { get; private set; }

        public string Title { get; private set; }
        public string Subtitle { get; private set; }

        public DateTime CreateAt { get; private set; }
        public DateTime UpdateAt { get; private set; }

        public List<Tag> Tags { get; private set; }

        public PreviewContent PreviewContent { get; private set; }
        public Delta Paragraphs { get; private set; }

        public Guid BlogerID { get; private set; }
        public Bloger Bloger { get; private set; }

        public void EditTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) throw new Exception("titile is null");
            Title = title;
        }

        public void EditSubTitle(string subTitle)
        {
            if (string.IsNullOrEmpty(subTitle)) throw new Exception("subtitle is null");
            Subtitle = subTitle;
        }

        public void UpdateParagraphs(Delta paragraphs)
        {
            if (paragraphs == null) throw new Exception("paragraphs is null");
            Paragraphs = paragraphs;
        }

        public void IsBloger(Guid blogerId)
        {
            BlogerID = blogerId;
        }
    }
}
