﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSBlogModel
{
    public class PreviewContent
    {
        public PreviewContent()
        {

        }

        public PreviewContent(string title, List<Tag> tags, string previewimage)
        {
            Title = title;
            PreviewImage = previewimage;
            Tags = tags;
        }

        [Key]
        public Guid PreviewContentID { get; set; }

        public string Title { get; private set; }
        public string PreviewImage { get; private set; }

        public List<Tag> Tags { get; private set; }

        public Guid ArticleID { get; private set; }
        public Article Article { get; private set; }

        public void ChangeBackgroupImage(string imgUrl)
        {
            // เช็คว่าไม่เท่ากับ Null รึป่าว
            PreviewImage = imgUrl;
        }

        public void UpdateTage(List<Tag> tags)
        {
            // check
            this.Tags = tags;
        }

        public void IsArticle(Guid articleID)
        {
            ArticleID = articleID;
        }
    }
}
