﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.Json.Serialization;

namespace CSBlogModel
{
    public class Bloger
    {
        public Bloger()
        {
            Profile = new Profile();
        }

        [Key]
        public Guid BlogerID { get; set; }
        public Profile Profile { get; set; }

        public List<Article> Blogs { get; private set; }

        public List<Subscribe> Followings { get; set; }
        public List<Subscribe> Followers { get; set; }

        public void AddBlog(ArticleRes acticle)
        {
            if (Blogs == null) Blogs = new List<Article>();
            Article blog = new Article(acticle.Title, acticle.Tages, acticle.SubTitle);
            Blogs.Add(blog);
        }
    }

    public class SubscribeRes
    {
        public Guid BlogerID { get; set; }
        public Guid FollowID { get; set; }
    }

    public class Subscribe
    {
        public Guid SubscribeID { get; private set; }

        public Guid BlogerID { get; private set; }
        public Bloger Bloger { get; private set; }

        public Guid FollowID { get; private set; }
        public Bloger Follow { get; private set; }

        public void FollowBloger(Guid blogerID, Guid followID)
        {
            if (blogerID == Guid.Empty) throw new Exception("blockger id is null");
            if (followID == Guid.Empty) throw new Exception("blockger id is null");

            BlogerID = blogerID;
            FollowID = followID;
        }
    }
}
