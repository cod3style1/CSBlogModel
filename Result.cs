﻿using System;
namespace CSBlogModel
{
    public class Result
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
