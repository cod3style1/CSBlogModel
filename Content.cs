﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CSBlogModel
{
    public class Content
    {
        public Content()
        {

        }

        [Key]
        public Guid ContentID { get; set; }

        public string Subtitle { get; private set; }
        public Delta Paragraphs { get; private set; }

        public Guid ArticleID { get; set; }
        public Article Article { get; set; }

        public void UpdateParagraphs(Delta paragraphs)
        {
            Paragraphs = paragraphs;
        }

        public Content(string subtitle)
        {
            Subtitle = subtitle;
            ContentID = Guid.NewGuid();
        }

        //+ InsertParagraph(..) :
        //+ EditParagraph(..) :
        //+ DeleteParagraph(..) :
    }
}
