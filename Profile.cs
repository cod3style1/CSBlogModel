﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CSBlogModel
{

    public class ProfileRes
    {
        public Guid ProfileID { get; set; }
        public string CodeName { get; set; }
        public string Bio { get; set; }
        public string Description { get; set; }
    }

    public class Profile
    {
        public Profile()
        {

        }

        public Profile(string codeName, string bio, string description)
        {
            CodeName = codeName;
            Bio = bio;
            Description = description;
        }

        [Key]
        public Guid ProfileID { get; private set; }
        public string CodeName { get; private set; }
        public string Bio { get; private set; }
        public string Description { get; private set; }

        public Guid BlogerID { get; set; }
        public Bloger Bloger { get; set; }

        public void UpdateProfile(string codeName, string bio, string description)
        {
            try
            {
                if (string.IsNullOrEmpty(codeName)) return;
                if (string.IsNullOrEmpty(bio)) return;
                if (string.IsNullOrEmpty(description)) return;

                CodeName = codeName;
                Bio = bio;
                Description = description;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
