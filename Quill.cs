﻿using System;
using System.Collections.Generic;

namespace CSBlogModel
{
    public class Delta
    {
        public int DeltaID { get; set; }
        public List<Op> Ops { get; set; }

        public Guid ArticleID { get; set; }
        public Article Article { get; set; }
    }

    public class Op
    {
        public int OpID { get; set; }
        public int Retain { get; set; }
        public string Insert { get; set; }
        public Attributes Attributes { get; set; }

        public int DeltaID { get; set; }
        public Delta Delta { get; set; }
    }

    public class Attributes
    {
        public int ID { get; set; }
        public string Link { get; set; }
        public string List { get; set; }
        public string Direction { get; set; }
        public string Align { get; set; }
        public string Size { get; set; }
        public string Script { get; set; }

        public int? Header { get; set; }
        public int? Indent { get; set; }

        public bool? Codeblock { get; set; }
        public bool? Bold { get; set; }
        public bool? Italic { get; set; }
        public bool? Underline { get; set; }
        public bool? Strike { get; set; }
        public bool? Blockquote { get; set; }

        public int OpID { get; set; }
        public Op Op { get; set; }
    }
}
