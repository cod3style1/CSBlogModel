﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace CSBlogModel
{
    public class Tag
    {
        [Key]
        public Guid TagID { get; set; }
        public string Name { get; set; }

        public Guid PreviewContentID { get; private set; }
        public PreviewContent PreviewContent { get; private set; }

        public Guid ArticleID { get; private set; }
        public Article Article { get; private set; }
    }
}
